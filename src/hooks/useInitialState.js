import { useState, useEffect } from 'react';

const useInitialState = (API) => {
  const [videos, setVideos] = useState([]);
  useEffect(() => {
    fetch(API)
      .then((response) => response.json())
      .then((data) => setVideos(data))
      .catch((err) => console.log(err));
  }, []);
  return videos;
};

export default useInitialState;

/*

CON ASYNC/AWAIT

useEffect(() => {
  const fetchVideos = async () => {
    try {
      const response = await fetch("http://localhost:3000/initialState");
      const data = await response.json();
      return setVideos(data);
    } catch (error) {
      console.log(error);
    }
  };
  fetchVideos();
}, []);

// modificar el .babelrc

{
  "presets": [
    [
      "@babel/preset-env",
      {
        "targets": {
          "esmodules": true
        }
      }
    ],
    "@babel/preset-react"
  ]
}

*/
